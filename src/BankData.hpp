// Copyright (C) 2020 Andrew Todd
#ifndef CPT_S_223_MA4_SRC_BANKDATA_HPP_
#define CPT_S_223_MA4_SRC_BANKDATA_HPP_

#include <iostream>
#include <memory>

class BankData {
 public:
  // Are the Big Five necessary?
  // How will this data work with the std::map?

  int getAcctNum() const;  // return the int, not the pointer
  double getSavingsAmount() const;  // return the double, not the pointer
  double getCheckingAmount() const;  // return the double, not the pointer

  void setAcctNum(const int& newAcctNum);  // you need to implement
  void setSavingsAmount(const double& newSavingsAmount);  // implement
  void setCheckingAmount(const double& newCheckingAmount);  // implement

 private:
  std::shared_ptr<int> mpAcctNum;
  std::shared_ptr<double> mpSavingsAmount, mpCheckingAmount;
};

#endif  // CPT_S_223_MA4_SRC_BANKDATA_HPP_

