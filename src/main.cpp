// Copyright (C) 2020 Andrew Todd

#include <unordered_map>
#include <fstream>
#include <sstream>
#include <string>
#include <utility>
#include <memory>

#include "BankData.hpp"

// EXPAND COMMENT BELOW FOR ANSWERS TO MA4 QUESTIONS!!!!!!!!!!!
/*
*     (4 pts) Assuming that the underlying hash table created cannot be resized, and that the hash function is poor, what is the worst-case Big-O for inserting key-value pairs based on a key? List any other assumptions that you make.
        The worst you can do is O(n);

*     (4 pts) Let’s say that the underlying hash table uses linear probing, what is the worst-case Big-O for retrieving data based on a key? List any assumptions that you make.
        The worst is O(n);

*     (4 pts) Let’s say the underlying hash table uses chaining, what is the worst-case Big-O for deleting key-value pairs based on a key? List any assumptions that you make.
        The worst is O(n);

*     (4 pts) What is the worst-case Big-O for iterating through the entire unordered map? List any assumptions that you make.
        Worst is O(n);

*     (4 pts) Based on your conclusions of the tasks that were performed in this assignment, when and why should we use an unordered map?
        When things need to be grouped and looked up quickly;

*     (10 pts) Is an std::unordered_map a robust choice for storing, removing, and searching bank accounts? Explain.
        It's not the best solution, but it was fairly easy to do. It took me like 2 hours to write this (5/5/20);
*/

void loadData(std::shared_ptr<std::unordered_map<int, BankData>> pam, std::shared_ptr<BankData> bank) {  // NOLINT
  std::fstream fin;
  std::string line, acc, save, check;
  fin.open("BankAccounts.csv");

  if (!fin.fail()) {
    while (std::getline(fin, line)) {
      std::istringstream iss(line);
      std::getline(iss, acc, ',');
      std::getline(iss, save, ',');
      std::getline(iss, check);
      bank->setAcctNum(stoi(acc));
      bank->setSavingsAmount(stod(save));
      bank->setCheckingAmount(stod(check));

      std::pair<int, BankData> account = std::make_pair(bank->getAcctNum(), *bank);  // NOLINT
      pam->insert(account);
      std::cout << "account " << account.first << " mapped to: " << pam->bucket(account.first) << std::endl;  // NOLINT
    }
  } else {
      std::cout << std::endl;
      std::cout << "File could not be opened!\n";
      std::cout << std::endl;
  }
  fin.close();
}

int main() {
  int del_key = 11111111;
  std::shared_ptr<BankData> bank(new BankData);
  std::shared_ptr<std::unordered_map<int, BankData>> pam (new std::unordered_map<int, BankData>);  // NOLINT
  loadData(pam, bank);

  std::cout << std::endl;
  std::cout << "Bucket Count: " << pam->bucket_count() << std::endl;  // NOLINT
  std::cout << "Container Size: " << pam->max_size() << std::endl;  // NOLINT
  std::cout << std::endl;

  for (auto &itPair : *pam) {
    std::cout << "Acc: " << itPair.first << " Savings: " << itPair.second.getSavingsAmount() << " Checking: " << itPair.second.getCheckingAmount() << std::endl;  // NOLINT
  }

  pam->erase(del_key);
  std::cout << std::endl;

  for (auto &itPair : *pam) {
    std::cout << "Acc: " << itPair.first << " Savings: " << itPair.second.getSavingsAmount() << " Checking: " << itPair.second.getCheckingAmount() << std::endl;  // NOLINT
  }

  std::cout << std::endl;
  std::cout << "Bucket Count: " << pam->bucket_count() << std::endl;  // NOLINT

  return 0;
}
