// Copyright (C) 2020 Andrew Todd
#include "BankData.hpp"

int BankData::getAcctNum() const {
  return *(this->mpAcctNum);
}

double BankData::getSavingsAmount() const {
  return *(this->mpSavingsAmount);
}

double BankData::getCheckingAmount() const {
  return *(this->mpCheckingAmount);
}

void BankData::setAcctNum(const int& newAcctNum) {
  mpAcctNum = std::make_shared<int>(newAcctNum);
}

void BankData::setSavingsAmount(const double& newSavingsAmount) {
  mpSavingsAmount = std::make_shared<double>(newSavingsAmount);
}

void BankData::setCheckingAmount(const double& newCheckingAmount) {
  mpCheckingAmount = std::make_shared<double>(newCheckingAmount);
}

